(function () {
    "use strict";
    angular
    .module("ParentConnectApp")
    .controller("AppController", AppController);
    
    AppController.$inject = ["$state", "$http"];

    function AppController($state, $http) {
        var AppControllerself  = this;
        
        AppControllerself.onSubmit = onSubmit;
        //AppControllerself.initForm = initForm;
        AppControllerself.onReset = onReset;
        
        AppControllerself.emailFormat = /^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/;
        AppControllerself.contactNumberFormat = /^[\s()+-]*([0-9][\s()+-]*){8}$/;
        AppControllerself.user = {
            
        }
        
        function onReset(){
            AppControllerself.user = Object.assign({}, AppControllerself.user);
            AppControllerself.registrationform.$setPristine();
            AppControllerself.registrationform.$setUntouched();
        }

        function onSubmit(){
            console.log(AppControllerself.user.email);
            $http.post("/api/register", AppControllerself.user).then((result)=>{
                AppControllerself.user = result.data;

            }).catch((error)=>{
                console.log("error > " + error);
            })
        }

        //AppControllerself.initForm();
    }
    
})();    