/**
 * Client side code.
 */
"use strict";
(function () {
    angular.module("ParentConnectApp", ["ui.router"]);
})();