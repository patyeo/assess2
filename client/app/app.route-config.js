(function () {
    angular
        .module("ParentConnectApp")
        .config(ParentConnectAppConfig);
        ParentConnectAppConfig.$inject = ["$stateProvider","$urlRouterProvider"];

    function ParentConnectAppConfig($stateProvider,$urlRouterProvider){
        $stateProvider
            
            .state("home", {
                url: "/home",
                templateUrl: "views/home.html",
                controller : 'AppController',
                controllerAs : 'ctrl'
            })

            .state("signin", {
                url: "/signin",
                templateUrl: "views/signin.html",
                controller : 'AppController',
                controllerAs : 'ctrl'
            })

            .state("signup", {
                url: "/signup",
                templateUrl: "views/form.html",
                controller : 'AddMemberCtrl',
                controllerAs : 'ctrl'
            })
            


        $urlRouterProvider.otherwise("/home");
    }

})();