(function(){
    angular
        .module("ParentConnectApp")
        .service("ParentConnectAppAPI", [
            '$http',
            ParentConnectAppAPI
        ]);
    
    function ParentConnectAppAPI($http){
        var self = this;

        /* query string '?string='
        self.searchEmployees = function(value){
            return $http.get("/api/employees?keyword=" + value);
        }*/
        //to query more than 1 item
        self.searchEvents = function(value, sortby, itemsPerPage, currentPage){
            return $http.get(`/api/events?keyword=${value}&sortby=${sortby}&itemsPerPage=${itemsPerPage}&currentPage=${currentPage}`);
        }

        // param request
        self.getEvent = function(evt_id){
            console.log(evt_id);
            return $http.get("/api/events/" + evt_id)
        }

        // body request
        self.updateEvent = function(event){
            console.log(event);
            return $http.put("/api/events", event);
        }
        // parameterized values
        /*
        self.searchEmployees = function(value){
            return $http.get("/api/employees/" + value);
        }*/

        // must learn and master this
//query and params use 'get'
//body use 'post'
// in request, there are 3 ways. one is to post from body, one is to get from parameters, one is by query string
        // post by body over request
        self.addEvent = function(event){
            return $http.post("/api/events", event);
        }
// here is requesting employee = the body of the data

        self.deleteEvent = function(evt_id){
            console.log(evt_id);
            return $http.delete("/api/events/"+ evt_id);
        }
    }
/* this is requesting in the form of parameters - the specific cell. 
be very clear if there is a / to specify the root folder/file to make changes to*/
})();
