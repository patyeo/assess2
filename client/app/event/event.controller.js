(function () {
    "use strict";
    angular.module("ParentConnectApp").controller("EventCtrl", EventCtrl);
    
    EventCtrl.$inject = ["$http"];

    function EventCtrl($http) {
        var EventCtrlself  = this;
        
        EventCtrlself.onSubmit = onSubmit;
        EventCtrlself.initForm = initForm;
        EventCtrlself.onReset = onReset;
        
//need to amend this code
        EventCtrlself.noKids = [
            { name: "Please select" , value:0},
            { name: "None", value:1},
            { name: "1", value: 2},
            { name: "2", value: 3},
            { name: "3", value: 4},
            { name: "4 & more", value: 5}
        ];

        function initForm(){
            regCtrlself.user.selectedNationality = "0";

        }

        function onReset(){
            regCtrlself.user = Object.assign({}, regCtrlself.user);
            regCtrlself.registrationform.$setPristine();
            regCtrlself.registrationform.$setUntouched();
        }

        function onSubmit(){
            console.log(regCtrlself.user.email);
            $http.post("/api/register", regCtrlself.user).then((result)=>{
                console.log("result > " + result);
                regCtrlself.user = result.data;
                console.log("Email > " + regCtrlself.user.email);

            }).catch((error)=>{
                console.log("error > " + error);
            })
        }



        regCtrlself.initForm();
    }
    
})();    