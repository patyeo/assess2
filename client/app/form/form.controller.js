(function () {
    "use strict";
    angular.module("ParentConnectApp").controller("AddMemberCtrl", AddMemberCtrl);
    
    AddMemberCtrl.$inject = ["$state", "$http"];

    function AddMemberCtrl($state, $http) {
        var AddMemberCtrlself  = this;
        
        AddMemberCtrlself.onSubmit = onSubmit;
        AddMemberCtrlself.initForm = initForm;
        AddMemberCtrlself.onReset = onReset;
        

        AddMemberCtrlself.emailFormat = /^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/;
        AddMemberCtrlself.contactNumberFormat = /^[\s()+-]*([0-9][\s()+-]*){8}$/;
        AddMemberCtrlself.pCodeFormat = /^(\d{6})$/;
        AddMemberCtrlself.user = {
            
        }

        AddMemberCtrlself.nationalities = [
            { name: "Please select" , value:0},
            { name: "Singaporean", value: 1},
            { name: "Malaysian", value: 2},
            { name: "Filipino", value: 3},
            { name: "Indonesian", value: 4},
            { name: "Others", value: 5}      
        ];

        AddMemberCtrlself.categories = [
            { name: "Please select" , value:0},
            { name: "Parenting - Pre-Schoolers", value: 1},
            { name: "Parenting - Primary-Schoolers", value: 2},
            { name: "Parenting - Teenagers", value: 3},
            { name: "Parenting - Adult Children", value: 4},
            { name: "Marriage", value: 5},
            { name: "Single Parents", value: 6},
            { name: "Spiritual Matters", value: 7}      
        ];

/*        AddMemberCtrlself.ages = [
            { name: "Please select" , value:0},
            { name: "Below 30 years old", value:1},
            { name: "30-39 years old", value: 2},
            { name: "40-49 years old", value: 3},
            { name: "50-59 years old", value: 4},
            { name: "60 years old & Above", value: 5}
        ];

        AddMemberCtrlself.noKids = [
            { name: "Please select" , value:0},
            { name: "None", value:1},
            { name: "1", value: 2},
            { name: "2", value: 3},
            { name: "3", value: 4},
            { name: "4 & more", value: 5}
        ];*/

        function initForm(){
            AddMemberCtrlself.user.selectedNationality = "0";
            AddMemberCtrlself.user.gender = "F";
            AddMemberCtrlself.user.selectedCategory = "0";
            //AddMemberCtrlself.user.selectedAge = "0";
            //AddMemberCtrlself.user.selectedNoKid = "0";
        }

        function onReset(){
            AddMemberCtrlself.user = Object.assign({}, regCtrlself.user);
            AddMemberCtrlself.registrationform.$setPristine();
            AddMemberCtrlself.registrationform.$setUntouched();
        }

        function onSubmit(){
            console.log("-----start onsubmit-----");
            console.log(AddMemberCtrlself.user.firstName);
            console.log(AddMemberCtrlself.user.email);
            console.log(AddMemberCtrlself.user.password);
            console.log(AddMemberCtrlself.user.confirmpassword);
            console.log(AddMemberCtrlself.user.contactNumber);
            console.log(AddMemberCtrlself.user.gender);
            console.log(AddMemberCtrlself.user.selectedNationality);
            console.log(AddMemberCtrlself.user.pCode);
            console.log(AddMemberCtrlself.user.category);
            $http.post("/api/member", AddMemberCtrlself.user).then((result)=>{
                console.log("result > " + result);
                AddMemberCtrlself.user = result.data;
                console.log("-----after http post-----");
                console.log("firstName > " + AddMemberCtrlself.user.firstName);
                console.log("Email > " + AddMemberCtrlself.user.email);
                console.log("password > " + AddMemberCtrlself.user.password);
                console.log("confirmpassword > " + AddMemberCtrlself.user.confirmpassword);
                console.log("gender > " + AddMemberCtrlself.user.gender);
                console.log("pCode > " + AddMemberCtrlself.user.pCode);
                console.log("selectedNationality > " + AddMemberCtrlself.user.selectedNationality);
                console.log("contactNumber > " + AddMemberCtrlself.user.contactNumber);
            }).catch((error)=>{
                console.log("error > " + error);
            })
        }



        AddMemberCtrlself.initForm();
    }
    
})();    