(function() {
    angular
      .module('ParentConnectApp')
      .controller('SigninCtrl', SigninCtrl);
  
    SigninCtrl.$inject = [ 'PassportSvc', '$state' ];
  
    function SigninCtrl(PassportSvc, $state) {
      var SigninCtrlself = this;
  
      SigninCtrlself.user = {
          username: "",
          password: ""
      };

      SigninCtrlself.msg = "";
      SigninCtrlself.login = login;

      function signin(){
        PassportSvc.signin(SigninCtrlself.user)
        .then(function(result) {
          $state.go('home');
          return true;
        })
        .catch(function(err) {
          SigninCtrlself.msg = 'Invalid Username or Password!';
          SigninCtrlself.user.username = SigninCtrlself.user.password = '';
          return false;
        });
      }
    }
  })();
