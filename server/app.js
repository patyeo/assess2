/** Server side code.
 */
"use strict";
console.log("Starting...");
var express = require("express");
var bodyParser = require("body-parser");
var Sequelize = require ("sequelize");

const EVENT_API = "/api/event";

const SQL_USERNAME = process.env.SQL_USERNAME;
const SQL_PASSWORD = process.env.SQL_PASSWORD;
var connection = new Sequelize(
    'parentconnect',
    SQL_USERNAME,
    SQL_PASSWORD,
    {
        host: 'localhost',
        port: 3306,
        logging: console.log,
        dialect: 'mysql',
        pool: {
            max: 10,
            min: 0,
            idle: 20000,
            acquire: 20000
        }
    }
);

var app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

console.log(__dirname);
console.log(__dirname + "/../client/");
const NODE_PORT = process.env.PORT | 3000;

var Members = require('./models/members')(connection, Sequelize);
const MEMBER_API = "/api/member";

app.post(MEMBER_API, (req, res)=>{
    var regMember = req.body;
    console.log(">> " + JSON.stringify(req.body)); 
    Members.create(member)
    .then((result)=>{
        console.log(result);
        res.status(200).json(result);
    }).catch((error)=>{
        console.log(error);
        res.status(500).json(error);
    });
});

/*    Members.findorCreate(req, res)({
        firstName: req.body.firstname,
        lastName: req.body.lastName,
        email: req.body.email,
        password: req.body.email,
        contact: req.body.contact,
        gender: req.body.gender,
        nationality: req.body.nationality,
        pCode: req.body.pCode,
        category: req.body.category,
        }).then(function (newMember) {
            res.status(200).json(newMember);
        })
        .catch(function (error){
            res.status(500).json(error);
        });*/

/*
Member.hasMany(Interest, {foreignKey: 'mbr_id'})
Event.belongsTo(Event_Interest, {foreignKey: ''});*/

/*Event.findOne().then(Event => {
    console.log(Event.get('event'));
});*/


/*console.log("Common Name > " + registeredUser.firstName);
console.log("contact Number > " + registeredUser.contactNumber);*/









app.use(express.static(__dirname + "/../client/"));

app.use(function (req, res) {
    res.send("<h1>Page not found</h1>");
});

app.listen(NODE_PORT, function () {
    console.log("Web App started at " + NODE_PORT);
});

module.exports = app;