// this is for event interest
module.exports.event_interest = function(connection, Sequelize){
    var EventInterests = connection.define('eventinterests', {
        evt_int_id: {
            type: Sequelize.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        evt_id: {
            type: Sequelize.INTEGER(11),
            allowNull: false,
            foreignKey: true
        },
        int_id: {
            type: Sequelize.INTEGER(11),
            allowNull: false,
            foreignKey: true
        },
        CreatedAt: {
            type: Sequelize.TIMESTAMP,
            allowNull: false
        },
        UpdatedAt: {
            type: Sequelize.TIMESTAMP,
            allowNull: false
        },
        isEnabled: {
            type: Sequelize.INTEGER(2),
            allowNull: true
        }
    }, {
        timestamps: false
    });
    return EventInterests;
}