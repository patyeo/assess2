// this is for events
module.exports.event = function(connection, Sequelize){
    var Events = connection.define('events', {
        evt_id: {
            type: Sequelize.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        title: {
            type: Sequelize.STRING(45),
            allowNull: false
        },
        article: {
            type: Sequelize.STRING(500),
            allowNull: false
        },
        venue:{
            type: Sequelize.STRING(100),
            allowNull: false
        },
        event_Date: {
            type: Sequelize.DATE,
            allowNull: false
        },
        event_Time: {
            type: Sequelize.TIME,
            allowNull: false
        },
        pCode: {
            type: Sequelize.INTEGER(6),
            allowNull: false
        },
        imgURL: {
            type: Sequelize.STRING(150),
            allowNull: true,
            defaulValue: "https://iamanthonychan.files.wordpress.com/2016/12/cropped-slide11.jpg"
        },
        pdfURL: {
            type: Sequelize.STRING(150),
            allowNull: true,
            defaulValue: "https://iamanthonychan.files.wordpress.com/2016/12/cropped-slide11.jpg"
        },
        isNotify: {
            type: Sequelize.INTEGER(2),
            allowNull: true
        }, 
        CreatedAt: {
            type: Sequelize.TIMESTAMP,
            allowNull: false
        },
        UpdatedAt: {
            type: Sequelize.TIMESTAMP,
            allowNull: false
        },
        isEnabled: {
            type: Sequelize.INTEGER(2),
            allowNull: true
        }
    }, {
        timestamps: false
    });
    return Events;
}

