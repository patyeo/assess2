// this is for interest
module.exports.interest = function(connection, Sequelize){
    var Interests = connection.define('events', {
        int_id: {
            type: Sequelize.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        description: {
            type: Sequelize.STRING(500),
            allowNull: false
        },
        category: {
            type: Sequelize.STRING(120),
            allowNull: false
        },
        category_id:{
            type: Sequelize.INTEGER(5),
            allowNull: false
        },
        CreatedAt: {
            type: Sequelize.TIMESTAMP,
            allowNull: false
        },
        UpdatedAt: {
            type: Sequelize.TIMESTAMP,
            allowNull: false
        },
        isEnabled: {
            type: Sequelize.INTEGER(2),
            allowNull: true
        }
    }, {
        timestamps: false
    });
    return Events;
}

