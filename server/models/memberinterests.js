// this is for member interest
module.exports.member_interest = function(connection, Sequelize){
    var MemberInterests = connection.define('memberinterests', {
        mbr_int_id: {
            type: Sequelize.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        mbr_id: {
            type: Sequelize.INTEGER(11),
            allowNull: false,
            foreignKey: true
        },
        int_id: {
            type: Sequelize.INTEGER(11),
            allowNull: false,
            foreignKey: true
        },
        CreatedAt: {
            type: Sequelize.TIMESTAMP,
            allowNull: false
        },
        UpdatedAt: {
            type: Sequelize.TIMESTAMP,
            allowNull: false
        },
        isEnabled: {
            type: Sequelize.INTEGER(2),
            allowNull: true
        }
    }, {
        timestamps: false
    });
    return MemberInterests;
}
